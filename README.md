# Sinatra on YAMLs

- Controller - Sinatra
- Persistance - YamlRecord
- View - Cells

## Change log

- 0.1.0 - Task renamed to Note. Full CRUD scaffold for it. YamlRecord instead of YAML::Store.
- 0.0.1 - Create, read, destroy scaffold for Task model.
